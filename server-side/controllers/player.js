const bcrypt = require('bcrypt')
const saltRounds = 10
const {Player} = require('../models')

class PlayerController{
    static async playerCreate(req, res){
        try{
            const {username, email, password, experience, lvl} = req.body

            const salt = bcrypt.genSaltSync(saltRounds)
            const hash = bcrypt.hashSync(password, salt)

            let inputPlayer = {
                username,
                email,
                password: hash,
                experience,
                lvl
            }
            const dataInput = await Player.create(inputPlayer)
            res.status(201).json({message: "Add new user succesed"})
        } catch(error){
            res.status(500).json(error)
        }
    }

    static async updatePlayer(req, res){
        try{
            const {username, email, password, experience, lvl} = req.body

            let inputPlayer = {
                username,
                email,
                password,
                experience,
                lvl
            }

            const dataInput = await Player.update(inputPlayer, {
                where:{
                    id: req.params.id
                }
            })

            res.status(200).json({msg: "Player Updated"})
        }catch(error){
            res.status(500).json(error)
        }
    }

    static async deletePlayer(req, res){
        try{
            const dataInput = await Player.destroy({
                where:{
                    id: req.params.id
                }
            })

            res.status(200).json({msg: "Player Deleted"})
        }catch(error){
            res.status(500).json(error)
        }
    }

    static async getPlayers(req, res){
        try {
            const response = await Player.findAll();
            res.status(200).json(response);
        } catch (error) {
            res.status(500).json(error)
        }
    }

    static async getPlayerById(req, res){
        try{
            const dataInput = await Player.findOne({
                where:{
                    id: req.params.id
                }
            })

            res.status(200).json({msg: "Player Updated"})
        }catch(error){
            res.status(500).json(error)
        }
    }

}   

module.exports = PlayerController