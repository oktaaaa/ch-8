const routes = require('express').Router()
const PlayerController = require('../controllers/player')

//endpoints
routes.get('/players', PlayerController.getPlayers)
routes.post('/players', PlayerController.playerCreate)
routes.patch('/players/', PlayerController.updatePlayer);
routes.get('/players/:id', PlayerController.getPlayerById)
routes.delete('/players/:id', PlayerController.deletePlayer)
module.exports = routes