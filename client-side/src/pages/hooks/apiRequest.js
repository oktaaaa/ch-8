import axios from "axios"

const baseUrl = "http://localhost:3000/players"

export function createPlayer(payload){
    
    return axios({
        method: 'post',
        url: baseUrl + '/playerCreate',
        data: payload
    })
}