import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import FormAddPlayer from './components/formAddPlayer';
import FormPlayer from './components/formPlayer';
import UpdatePlayer from './components/formUpdatePlayer';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<FormPlayer/>}/>
        <Route path="add" element={<FormAddPlayer/>}/>
        <Route path = "edit/:id" element= {<UpdatePlayer/>}/>
        
      </Routes>
    </BrowserRouter>
  );
}

export default App;
