import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

export default function FormPlayer(){
  const [players, setPlayer] = useState([])

  useEffect(() => {
    getPlayers()
  }, [])

  const getPlayers = async() =>{
    const response = await axios.get("http://localhost:3000/players")
    setPlayer(response.data)
  }

  const deletePlayer = async(id) =>{
    try{
      await axios.delete(`http://localhost:3000/players/${id}`)
      getPlayers()
    }catch(error){
      console.log(error);
    }
  }

    return (
        <div className="columns mt-5 is-centered">
          <div className="column is-half">
          <Link to={`add`} className="button is-success">
          Add New
          </Link>
            <table className="table is-striped is-fullwidth">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Experience</th>
                  <th>Level</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                {players.map((player, index)=>(
                  <tr key = {player.id}>
                    <td>{player.id}</td>
                    <td>{player.username}</td>
                    <td>{player.email}</td>
                    <td>{player.password}</td>
                    <td>{player.experience}</td>
                    <td>{player.lvl}</td>
                    <td>
                        <Link 
                            to={`edit/${player.id}`}
                            className="button is-small is-info mr-2"
                        >
                            Edit
                        </Link>
                        <button
                            onClick={() => deletePlayer(player.id)}
                            className="button is-small is-danger"
                        >
                            Delete
                        </button>
                      </td>
                     
                  </tr> 
                ))}
              </tbody>
            </table>
          </div>
        </div>
      );
}