import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

export default function UpdatePlayer(){
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [experience, setExperience] = useState('')
  const [lvl, setLevel] = useState('')
    const { id } = useParams()

    

    const navigate = useNavigate()

    useEffect(() => {
        getPlayerById()
    }, [])

    const updatePlayer = async(e) =>{
        e.preventDefault()
        try{
            await axios.patch(`http://localhost:3000/players/${id}`, {
                username,
                email,
                password,
                experience,
                lvl
            })
            navigate('/')
        } catch (error){
            console.log(error);
        }
    }

    const getPlayerById = async()=> {
        const response = await axios.get(`http://localhost:3000/players/${id}`)
        setUsername(response.data.username)
        setEmail(response.data.email)
        setPassword(response.data.password)
        setExperience(response.data.experience)
        setLevel(response.data.lvl)
    }

    return (
        <div className="columns mt-5 is-centered">
          <div className="column is-half">
            <form onSubmit={updatePlayer}>
              <div className="field">
                <label className="label">Username</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    value = {username}
                    onChange = {(e) => setUsername(e.target.value)}
                    placeholder="Username"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Email</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    value = {email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Password</label>
                <div className="control">
                  <input
                    type="password"
                    className="input"
                    value = {password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Password"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Experience</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    value = {experience}
                    onChange={(e) => setExperience(e.target.value)}
                    placeholder="Experience"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Level</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    value = {lvl}
                    onChange={(e) => setLevel(e.target.value)}
                    placeholder="Level"
                  />
                </div>
              </div>
              <div className="field">
                <button type="submit" className="button is-success">
                  Update
                </button>
              </div>
            </form>
          </div>
        </div>
      );
}